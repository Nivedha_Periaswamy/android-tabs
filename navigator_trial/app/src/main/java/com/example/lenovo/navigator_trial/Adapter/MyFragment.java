package com.example.lenovo.navigator_trial.Adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.example.lenovo.navigator_trial.Adapter.Activity.R;

/**
 * Created by LENOVO on 6/14/2015.
 */
public class MyFragment extends Fragment {

    private TextView textView;
    private TabHost tabHost;

    public static MyFragment get_instance(int position){
        MyFragment obj=new MyFragment();
        Bundle args=new Bundle();
        args.putInt("position",position);
        obj.setArguments(args);
        return obj;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout= inflater.inflate(R.layout.home, container, false);
        textView=(TextView)layout.findViewById(R.id.textView);
        Bundle bundle=getArguments();
       /* for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            View v = tabHost.getTabWidget().getChildAt(i);
            //  v.setBackgroundResource(R.drawable.tabs);

            TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getResources().getColor(R.color.textColorPrimary));
        }*/
        if(bundle!=null)
        {
            textView.setText(Integer.toString(bundle.getInt("position")));
        }

        // Inflate the layout for this fragment
        return layout;
    }

}
