package com.example.lenovo.navigator_trial.Adapter.Activity;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;


import com.example.lenovo.navigator_trial.Adapter.Fragment_Drawer;
import com.example.lenovo.navigator_trial.Adapter.MyFragment;
import com.example.lenovo.navigator_trial.Adapter.SlidingTabLayout;
//import com.example.lenovo.navigator_trial.Adapter.TabsPagerAdapter;


public class MyAppActivity extends AppCompatActivity implements Fragment_Drawer.FragmentDrawerListener{

        public Toolbar obj_tool;
        public Fragment_Drawer frag_draw_obj;
        private ViewPager viewPager;
        private TabsPagerAdapter mAdapter;
        private ActionBar actionBar;
       // private String[] tabs = { "Messages", "Calls", "Data" };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_app);
        obj_tool = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(obj_tool);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        frag_draw_obj = (Fragment_Drawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        frag_draw_obj.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), obj_tool);
        frag_draw_obj.setDrawerListener(this);
        displayView(0);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onDrawerItemSelected(View view, int position) {
            displayView(position);
    }
    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new Home();
                title = getString(R.string.title_home);
                viewPager=(ViewPager)findViewById(R.id.pager);
                viewPager.setAdapter(new TabsPagerAdapter(getSupportFragmentManager()));
                SlidingTabLayout mtabs=(SlidingTabLayout)findViewById(R.id.tab);
                mtabs.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mtabs.setSelectedIndicatorColors(getResources().getColor(R.color.colorPrimaryDark));
            //    TextView tv = (TextView) TabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
              //  tv.setTextColor(Color.parseColor("#ffffff"));
                mtabs.setViewPager(viewPager);
                break;
            case 1:
                fragment = new about();
                title = getString(R.string.title_about);
                break;
            case 2:
                fragment = new Rateus();
                title = getString(R.string.title_rateus);
                break;
            case 3:
                fragment= new faq();
                title=getString(R.string.title_faq);
            case 4:
                fragment= new Feedback();
                title=getString(R.string.title_feedback);

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }
    
}

class  TabsPagerAdapter extends FragmentPagerAdapter{

        String tabs[];
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        MyFragment obj=MyFragment.get_instance(position);
        return obj;
    }
    public CharSequence getTab_Title(int position)
    {
      return tabs[position];
    }
    @Override
    public int getCount() {
        return 3;
    }
}

