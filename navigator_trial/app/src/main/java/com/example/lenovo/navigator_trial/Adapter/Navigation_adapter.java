package com.example.lenovo.navigator_trial.Adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.Collections;
import java.util.List;

import com.example.lenovo.navigator_trial.Adapter.Activity.R;
import com.example.lenovo.navigator_trial.Adapter.model.NavDrawer;

/**
 * Created by LENOVO on 6/10/2015.
 */

public class Navigation_adapter extends RecyclerView.Adapter<Navigation_adapter.MyViewHolder> {
    List<NavDrawer> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public Navigation_adapter(Context context, List<NavDrawer> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.navigation_drawer, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawer current = data.get(position);
        holder.title.setText(current.getTitle());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}